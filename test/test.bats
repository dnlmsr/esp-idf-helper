setup() {
    bats_load_library bats-support
    bats_load_library bats-assert
    DIR="$(cd "$(dirname "$BATS_TEST_FILENAME")" >/dev/null 2>&1 && pwd)"
    PATH="$DIR/../src:$PATH"
    export HOME=/tmp
    export XDG_DATA_HOME="$HOME/.local/share"
}

teardown() {
    rm -rf "/tmp/.local"
}

@test "create esp-idf directory" {
    esp-idf-helper v5.2.1
    assert [ -d ~/.local/share/esp-idf ]
}

@test "clone bare repo" {
    esp-idf-helper v5.2.1
    assert [ -d ~/.local/share/esp-idf/repo.git/objects ]
}

@test "create worktree directory" {
    esp-idf-helper v5.2.1

    assert [ -e ~/.local/share/esp-idf/v5.2.1/install.sh ]
    assert [ -e ~/.local/share/esp-idf/v5.2.1/export.sh ]
}

@test "check correct installation" {
    esp-idf-helper v5.2.1

    assert [ -d ~/.local/share/espressif/v5.2.1 ]
}

@test "check app recall" {
    esp-idf-helper v5.2.1
    esp-idf-helper v5.2.1
}

@test "check without xdg var" {
    unset XDG_DATA_HOME
    esp-idf-helper v5.2.1

    assert [ -d ~/.local/share/espressif/v5.2.1 ]
}

@test "check fetch when tag not present" {
    esp-idf-helper v5.2.1

    git --git-dir="${XDG_DATA_HOME}/esp-idf/repo.git" tag --delete v5.4

    esp-idf-helper v5.4
    assert [ -e ~/.local/share/esp-idf/v5.4/install.sh ]
    assert [ -e ~/.local/share/esp-idf/v5.4/export.sh ]
}
