# esp-idf-helper

## Description

`esp-idf-helper` is a shell script which aims to be a helper for installing and using multiple versions of Espressif's [esp-idf](https://github.com/espressif/esp-idf) framework.

## Dependencies

- git

## Features

- 🧹 Follows [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html)
- 🔁 Easy switch between esp-idf versions

## Usage

Script must be sourced in order to export the esp-idf variables properly. For example, to install and use v5.2.1 of the esp-idf framework just type:

```shell
. esp-idf-helper v5.2.1
```

And if you wish to switch to an older version you can just open a new shell and use:

```shell
. esp-idf-helper v5.1.2
```

## Known issues

- Doesn't work with release branches or refs that contains weird characters.
